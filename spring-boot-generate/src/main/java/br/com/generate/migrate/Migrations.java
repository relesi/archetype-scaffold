package br.com.generate.migrate;

/**
 * Generate migrations
 * @author Renato Lessa
 * @since 0.0.1
 */


public class Migrations {
	
	public void create(String className, String parametersModel) throws Exception {
		GenerateMigration.createMigrationFromModel(className, parametersModel);
	}
}
